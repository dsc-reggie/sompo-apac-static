(function(app) { 
	'use strict'; 
	
    var Menu = function() {};
    app.isTouchDevice = ('ontouchstart' in window);
    
	Menu.prototype.init = function() {
        var _self = this;
            _self.mainMenu();
            _self.backToTop();
            _self.mobileMenu();
    };
    Menu.prototype.mainMenu = function(){
            var _menuItem = $('.menu-item');

            _menuItem.hover(handlerIn, handlerOut);

            function handlerIn() {
                var _this = $(this);
                if(!_this.hasClass('active')) {
                    var _gif = _this.find('.animated-gif').attr('gif') + '?='+(+new Date());
                    _this.find('.animated-gif').attr('src',_gif).show();

                    if(app.isTouchDevice) {
                        window.location.href = _this.find('a').attr('href');
                    }
                }
            }

            function handlerOut() {
                var _this = $(this);
                if(!_this.hasClass('active')) {
                    _this.find('.animated-gif').attr('src','').hide();
                }
            }
            
            var _activeGif = $('.menu-item.active').find('.animated-gif').attr('gif') + '?='+(+new Date());
            $('.menu-item.active').find('.animated-gif').attr('src',_activeGif).show();
    }
    Menu.prototype.backToTop = function(){
        var _btn = $('.btn__back-to-top');

        _btn.on('click', function(e){
            e.preventDefault();
            // $(window).scrollTop(0);
            TweenMax.to(window, 1, {scrollTo:{y:0}, ease:Power2.easeInOut});
        });
    }
    Menu.prototype.mobileMenu = function(){
        var $burger = $('.burger');
        var $mobileHeader = $('.mobile-header');
        var $mobileMenuItem = $('.mobile-menu-item');
        $burger.on('click', function(){
            
            if (!$mobileHeader.hasClass('is-active')){
                TweenMax.staggerTo($mobileMenuItem, .3, {x: 0, delay: .3, opacity: 1,ease: Power3.easeOut}, 0.05)
                $mobileHeader.addClass('is-active');
            }else{
                TweenMax.staggerTo($mobileMenuItem, .3, {x: 100,opacity: 0,ease: Power3.easeOut,
                onComplete: function(){
                    $mobileHeader.removeClass('is-active');
                }}, 0.05)
                
            }
        })
    }

    app.Menu = Menu;

    app.ready(function () {
        Menu.prototype.init()
    })

})(window.app);